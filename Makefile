ARCHS = armv7 arm64

include theos/makefiles/common.mk

TWEAK_NAME = safariuploader
safariuploader_FILES = Tweak.x
safariuploader_FRAMEWORKS = UIKit
safariuploader_LDFLAGS = -Llibfinder -lfinder

include $(THEOS_MAKE_PATH)/tweak.mk
