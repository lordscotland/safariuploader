#include "libfinder/LFFinderController.h"
#include "libfinder/LFTemporaryFile.h"

@interface WKFileUploadPanel : UIViewController <LFFinderActionDelegate>
-(void)_cancel;
-(void)_chooseFiles:(NSArray*)files displayString:(NSString*)string iconImage:(UIImage*)image;
@end

@interface SU_LFFinderController : LFFinderController @end
@implementation SU_LFFinderController
-(void)dismiss {
  [super dismiss];
  WKFileUploadPanel* panel=(id)self.actionDelegate;
  if(panel){
    [panel retain];
    [panel _cancel];
    [panel release];
  }
}
@end

static SEL cmdPresentFullscreen;
static SEL cmdPresentPopover;
static void (*$$_WKFileUploadPanel_presentFullscreen)(id,SEL,id,BOOL);
static void (*$$_WKFileUploadPanel_presentPopover)(id,SEL,id,BOOL);
static void $_WKFileUploadPanel_present(WKFileUploadPanel* self,SEL _cmd,UIAlertController* alert,BOOL animated) {
  void (*orig)(id,SEL,id,BOOL)=(_cmd==cmdPresentPopover)?
   $$_WKFileUploadPanel_presentPopover:$$_WKFileUploadPanel_presentFullscreen;
  if([alert isKindOfClass:[UIAlertController class]]){
    [alert addAction:[UIAlertAction actionWithTitle:
     [[NSBundle bundleWithIdentifier:@"com.apple.WebCore"]
     localizedStringForKey:@"Choose File" value:nil table:nil]
     style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
      LFFinderController* finder=[[SU_LFFinderController alloc] init];
      finder.actionDelegate=self;
      orig(self,_cmd,(id)finder,animated);
      [finder release];
    }]];
  }
  orig(self,_cmd,alert,animated);
}

%hook WKFileUploadPanel
%new(@:@@)
-(void)finder:(LFFinderController*)finder didSelectItemsAtPaths:(NSArray*)paths {
  finder.actionDelegate=nil;
  [finder dismiss];
  NSString* displayString=nil;
  NSMutableArray* files=[NSMutableArray array];
  NSMutableArray* URLs=[NSMutableArray array];
  for (NSString* path in paths){
    LFTemporaryFile* file=[[LFTemporaryFile alloc] initWithPath:path forWriting:NO];
    if(file){
      [files addObject:file];
      [URLs addObject:[NSURL fileURLWithPath:file.path]];
      [file release];
      if(!displayString){displayString=path.lastPathComponent;}
    }
  }
  static Ivar var=NULL;
  if(!var){var=class_getInstanceVariable(%c(WKFileUploadPanel),"_view");}
  objc_setAssociatedObject(object_getIvar(self,var),self,files,
   OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  [self retain];
  [self _chooseFiles:URLs displayString:
   (URLs.count>1)?[NSString localizedStringWithFormat:
   [[NSBundle bundleWithIdentifier:@"com.apple.WebCore"]
   localizedStringForKey:@"%d files" value:nil table:nil],(int)files.count]:
   displayString iconImage:nil];
  [self release];
}
-(void)_showPhotoPickerWithSourceType:(UIImagePickerControllerSourceType)type {
  static ptrdiff_t offset=0;
  if(!offset){offset=ivar_getOffset(class_getInstanceVariable(%c(WKFileUploadPanel),"_allowMultipleFiles"));}
  if(*(BOOL*)((void*)self+offset)){
    UIAlertController* alert=[UIAlertController alertControllerWithTitle:nil
     message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:
     [[NSBundle bundleWithIdentifier:@"com.apple.WebCore"]
     localizedStringForKey:@"Photo Library (file upload action sheet)" value:nil table:nil]
     style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){%orig;}]];
    SEL cmd;
    if(alert.popoverPresentationController){cmd=cmdPresentPopover;}
    else {
      cmd=cmdPresentFullscreen;
      [alert addAction:[UIAlertAction actionWithTitle:
       [[NSBundle bundleWithIdentifier:@"com.apple.UIKit"]
       localizedStringForKey:@"Cancel" value:nil table:nil]
       style:UIAlertActionStyleCancel handler:^(UIAlertAction* action){[self _cancel];}]];
    }
    $_WKFileUploadPanel_present(self,cmd,alert,YES);
  }
  else {%orig;}
}
%end

%ctor {
  %init;
  MSHookMessageEx(%c(WKFileUploadPanel),
   cmdPresentPopover=@selector(_presentPopoverWithContentViewController:animated:),
   (IMP)$_WKFileUploadPanel_present,(IMP*)&$$_WKFileUploadPanel_presentPopover);
  MSHookMessageEx(%c(WKFileUploadPanel),
   cmdPresentFullscreen=@selector(_presentFullscreenViewController:animated:),
   (IMP)$_WKFileUploadPanel_present,(IMP*)&$$_WKFileUploadPanel_presentFullscreen);
}
