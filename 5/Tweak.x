#import <CoreText/CoreText.h>
#include "substrate.h"
#include "libfinder/LFClient.h"
#include "libfinder/LFFinderController.h"

extern CFReadStreamRef _CFReadStreamCreateFromFileDescriptor(CFAllocatorRef,int);

@interface EXTERNAL
+(id)sharedBrowserController;
@end

static BOOL $_ignoreDisable=NO,$_proxyOpen=YES;
static void* $_notFileUpload=NULL;
static void* $_wcRenderObject=NULL;

static void* (*$$_fastMalloc)(unsigned long);
static void* (*$$_String_init)(void*,NSString*);
static CFStringRef (*$$_String_createCFString)(void*);
static void* (*$$_File_init)(void*,void*);
static bool $_FileInputType_isFileUpload(void* self) {
  return self!=$_notFileUpload;
}
static void* (*$$_HTMLInputElement_files)(void*);
static bool (*$$_HTMLInputElement_disabled)(void*);
static bool $_HTMLInputElement_disabled(void* self) {
  $_notFileUpload=*(void**)(self+116);
  bool $_=$$_HTMLInputElement_disabled(self);
  $_notFileUpload=NULL;
  return $_;
}
static void (*$$_HTMLInputElement_setDisabled)(void*,bool);
static void $_HTMLInputElement_setDisabled(void* self,bool disabled) {
  if($_ignoreDisable){return;}
  $_notFileUpload=*(void**)(self+116);
  $$_HTMLInputElement_setDisabled(self,disabled);
  $_notFileUpload=NULL;
}
static void (*$$_HTMLInputElement_updateType)(void*);
static void $_HTMLInputElement_updateType(void* self) {
  $_ignoreDisable=YES;
  $$_HTMLInputElement_updateType(self);
  $_ignoreDisable=NO;
}
static void (*$$_HTMLInputElement_dispatchFormControlChangeEvent)(void*);
static void* (*$$_FileList_item)(void*,unsigned int);
static void (*$$_FileList_append)(void*,void**);
static void (*$$_Vector_shrinkCapacity)(void*,unsigned long);
static void (*$$_RenderObject_repaint)(void*,bool);
static int (*$$_RenderBox_offsetHeight)(void*);
static int (*$$_RenderBoxModelObject_borderLeft)(void*);
static int (*$$_RenderBoxModelObject_paddingLeft)(void*,bool);
static void (*$$_GraphicsContext_setFillColor)(void*,void*,int);
static void (*$$_FontPlatformData_init)(void*,CTFontRef,float,bool,bool,int,int,int);
static void (*$$_FontPlatformData_dealloc)(void*);
static void (*$$_Font_init)(void*,void*,bool,int);
static void (*$$_Font_drawText)(void*,void*,void*,CGPoint*,int,int);
static void $_RenderFileUploadControl_click(void* self) {
  $_wcRenderObject=self;
  static Ivar var=NULL;
  if(!var){var=class_getInstanceVariable(%c(BrowserController),"_rootViewController");}
  dispatch_async(dispatch_get_main_queue(),^{
    id browser=[%c(BrowserController) sharedBrowserController];
    LFFinderController* finder=[[LFFinderController alloc] init];
    finder.actionDelegate=browser;
    [object_getIvar(browser,var) presentViewController:finder animated:YES completion:NULL];
    [finder release];
  });
}
static void (*$$_RenderFileUploadControl_paintObject)(void*,void*,int,int);
static void $_RenderFileUploadControl_paintObject(void* self,void* info,int tx,int ty) {
  $$_RenderFileUploadControl_paintObject(self,info,tx,ty);
  if(*(int*)(info+20)==4){
    static char wcFont[40];
    static dispatch_once_t once;
    dispatch_once(&once,^{
      CTFontRef ctFont=CTFontCreateWithName(CFSTR(".HelveticaNeueUI"),12,NULL);
      char wcFontData[40];
      $$_FontPlatformData_init(wcFontData,ctFont,CTFontGetSize(ctFont),false,false,0,0,0);
      $$_Font_init(wcFont,wcFontData,false,0);
      $$_FontPlatformData_dealloc(wcFontData);
      CFRelease(ctFont);
    });
    struct {
      UniChar* text;
      int length;
      int flags[60];
    } wcTextRun={.flags[3]=0x3f800000,.flags[5]=1};
    void* wcFile=$$_FileList_item($$_HTMLInputElement_files(*(void**)(self+8)),0);
    if(wcFile){
      CFStringRef text=$$_String_createCFString(wcFile+72);
      wcTextRun.length=CFStringGetLength(text);
      CFStringGetCharacters(text,CFRangeMake(0,wcTextRun.length),
       wcTextRun.text=malloc(wcTextRun.length*2));
      CFRelease(text);
    }
    else {
      wcTextRun.text=(UniChar[]){'-','-','-'};
      wcTextRun.length=3;
    }
    void* wcContext=*(void**)info;
    $$_GraphicsContext_setFillColor(wcContext,&(char[]){0,0,0,255,1},0);
    $$_Font_drawText(wcFont,wcContext,&wcTextRun,&(CGPoint){
     tx+$$_RenderBoxModelObject_borderLeft(self)+$$_RenderBoxModelObject_paddingLeft(self,true)+86,
     ty+$$_RenderBox_offsetHeight(self)/2.+4},0,-1);
    if(wcFile){free(wcTextRun.text);}
  }
}
static void (*$$_RenderFileUploadControl_dealloc)(void*);
static void $_RenderFileUploadControl_dealloc(void* self) {
  if($_wcRenderObject==self){$_wcRenderObject=NULL;}
  $$_RenderFileUploadControl_dealloc(self);
}
static CFReadStreamRef (*$$_CFReadStreamCreateWithFile)(CFAllocatorRef,CFURLRef);
static CFReadStreamRef $_CFReadStreamCreateWithFile(CFAllocatorRef allocator,CFURLRef URL) {
  if([[NSThread currentThread].name isEqualToString:@"WebCore: CFNetwork Loader"]){
    CFStringRef path=CFURLCopyFileSystemPath(URL,kCFURLPOSIXPathStyle);
    NSFileHandle* handle=[LFClient openFileAtPath:(NSString*)path mode:"rb" error:NULL];
    CFRelease(path);
    if(handle){
      CFReadStreamRef stream=_CFReadStreamCreateFromFileDescriptor(allocator,handle.fileDescriptor);
      objc_setAssociatedObject((NSInputStream*)stream,&$_proxyOpen,
       handle,OBJC_ASSOCIATION_RETAIN_NONATOMIC);
      [handle release];
      return stream;
    }
  }
  return $$_CFReadStreamCreateWithFile(allocator,URL);
}

%hook BrowserController
%new
-(void)finder:(LFFinderController*)finder didSelectItemAtPath:(NSString*)path {
  [finder dismiss];
  if(!$_wcRenderObject){return;}
  void* wcInput=*(void**)($_wcRenderObject+8);
  void* wcFileList=$$_HTMLInputElement_files(wcInput);
  $$_Vector_shrinkCapacity(wcFileList+4,0);
  void* wcPath;
  $$_String_init(&wcPath,path);
  void* wcFile=$$_fastMalloc(76);
  $$_File_init(wcFile,&wcPath);
  $$_FileList_append(wcFileList,&wcFile);
  $$_HTMLInputElement_dispatchFormControlChangeEvent(wcInput);
  $$_RenderObject_repaint($_wcRenderObject,true);
}
%end

%ctor {
  %init;
  MSImageRef lib;
  lib=MSGetImageByName("/System/Library/PrivateFrameworks/JavaScriptCore.framework/JavaScriptCore");
  $$_fastMalloc=MSFindSymbol(lib,"__ZN3WTF10fastMallocEm");
  lib=MSGetImageByName("/System/Library/PrivateFrameworks/WebCore.framework/WebCore");
  $$_String_init=MSFindSymbol(lib,"__ZN3WTF6StringC1EP8NSString");
  $$_String_createCFString=MSFindSymbol(lib,"__ZNK3WTF6String14createCFStringEv");
  $$_File_init=MSFindSymbol(lib,"__ZN7WebCore4FileC1ERKN3WTF6StringE");
  $$_HTMLInputElement_files=MSFindSymbol(lib,"__ZN7WebCore16HTMLInputElement5filesEv");
  $$_HTMLInputElement_dispatchFormControlChangeEvent=MSFindSymbol(lib,"__ZN7WebCore22HTMLFormControlElement30dispatchFormControlChangeEventEv");
  $$_FileList_item=MSFindSymbol(lib,"__ZNK7WebCore8FileList4itemEj");
  $$_FileList_append=MSFindSymbol(lib,"__ZN7WebCore8FileList6appendEN3WTF10PassRefPtrINS_4FileEEE");
  $$_Vector_shrinkCapacity=MSFindSymbol(lib,"__ZN3WTF6VectorINS_6RefPtrIN7WebCore4FileEEELm0EE14shrinkCapacityEm");
  $$_RenderObject_repaint=MSFindSymbol(lib,"__ZN7WebCore12RenderObject7repaintEb");
  $$_RenderBox_offsetHeight=MSFindSymbol(lib,"__ZNK7WebCore9RenderBox12offsetHeightEv");
  $$_RenderBoxModelObject_borderLeft=MSFindSymbol(lib,"__ZNK7WebCore20RenderBoxModelObject10borderLeftEv");
  $$_RenderBoxModelObject_paddingLeft=MSFindSymbol(lib,"__ZNK7WebCore20RenderBoxModelObject11paddingLeftEb");
  $$_GraphicsContext_setFillColor=MSFindSymbol(lib,"__ZN7WebCore15GraphicsContext12setFillColorERKNS_5ColorENS_10ColorSpaceE");
  $$_FontPlatformData_init=MSFindSymbol(lib,"__ZN7WebCore16FontPlatformDataC2EPK8__CTFontfbbNS_15FontOrientationENS_15TextOrientationENS_16FontWidthVariantE");
  $$_FontPlatformData_dealloc=MSFindSymbol(lib,"__ZN7WebCore16FontPlatformDataD1Ev");
  $$_Font_init=MSFindSymbol(lib,"__ZN7WebCore4FontC1ERKNS_16FontPlatformDataEbNS_17FontSmoothingModeE");
  $$_Font_drawText=MSFindSymbol(lib,"__ZNK7WebCore4Font8drawTextEPNS_15GraphicsContextERKNS_7TextRunERKNS_10FloatPointEii");
  MSHookFunction(MSFindSymbol(lib,"__ZNK7WebCore13FileInputType12isFileUploadEv"),
   $_FileInputType_isFileUpload,NULL);
  MSHookFunction(MSFindSymbol(lib,"__ZNK7WebCore16HTMLInputElement8disabledEv"),
   $_HTMLInputElement_disabled,(void*)&$$_HTMLInputElement_disabled);
  MSHookFunction(MSFindSymbol(lib,"__ZN7WebCore16HTMLInputElement11setDisabledEb"),
   $_HTMLInputElement_setDisabled,(void*)&$$_HTMLInputElement_setDisabled);
  MSHookFunction(MSFindSymbol(lib,"__ZN7WebCore16HTMLInputElement10updateTypeEv"),
   $_HTMLInputElement_updateType,(void*)&$$_HTMLInputElement_updateType);
  MSHookFunction(MSFindSymbol(lib,"__ZN7WebCore23RenderFileUploadControl5clickEv"),
   $_RenderFileUploadControl_click,NULL);
  MSHookFunction(MSFindSymbol(lib,"__ZN7WebCore23RenderFileUploadControl11paintObjectERNS_9PaintInfoEii"),
   $_RenderFileUploadControl_paintObject,(void*)&$$_RenderFileUploadControl_paintObject);
  MSHookFunction(MSFindSymbol(lib,"__ZN7WebCore23RenderFileUploadControlD0Ev"),
   $_RenderFileUploadControl_dealloc,(void*)&$$_RenderFileUploadControl_dealloc);
  MSHookFunction(CFReadStreamCreateWithFile,
   $_CFReadStreamCreateWithFile,(void*)&$$_CFReadStreamCreateWithFile);
}
